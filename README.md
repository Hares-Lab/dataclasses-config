# DataClasses Config

A Python package which extends [pyhocon](https://pypi.org/project/pyhocon/)
package's functionality with the automatically-parsed dataclasses.
