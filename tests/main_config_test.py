from typing import *
from unittest import main

from dataclasses_config.backports import cached_property
from dataclasses_config.config import main_config, MainConfig
from tests.helpers.classes import *
from tests.helpers.config_assertions import ConfigAssertionsMixin


@main_config()
class SampleConfig(MainConfig):
    s1: str
    b1: bool
    b2: bool
    i1: int
    f1: float
    o1: Dict[str, Any]
    l1: List[int]
    
    l2: List[Circle]
    c1: Circle
    c2: Circle

C = TypeVar('C')
T = TypeVar('T')
class MainConfigClassTestCase(ConfigAssertionsMixin):
    
    def assertSampleConfigEqual(self, actual: SampleConfig, expected: SampleConfig):
        self.assertConfigEqual(actual, expected, cfg_cls=SampleConfig)
    
    @cached_property
    def expected_application_conf(self) -> SampleConfig:
        app_conf = SampleConfig \
        (
            # Built-in types
            s1 = "some string",
            b1 = False,
            b2 = True,
            i1 = 1543,
            f1 = -555.23,
            o1 = dict(f1="field 1", f2="field 2"),
            l1 = [ 1, 5, 10 ],
            
            # User types
            l2 =
            [
                Circle(center=Point(0, 0), radius=5),
                Circle(center=Point(1, 1), radius=3),
            ],
            c1 = Circle(center=Point(2, -3), radius=10),
            c2 = Circle(center=Point(0, 2), radius=1),
        )
        
        return app_conf
    @cached_property
    def expected_reference_conf(self) -> SampleConfig:
        app_conf = SampleConfig \
        (
            # Built-in types
            s1 = "default string",
            b1 = False,
            b2 = False,
            i1 = 0,
            f1 = 0.0,
            o1 = dict(),
            l1 = [ ],
            
            # User types
            l2 = [ ],
            c1 = Circle(center=Point(0, 0), radius=1),
            c2 = Circle(center=Point(0, 0), radius=2),
        )
        
        return app_conf
    
    @property
    def application_conf(self) -> SampleConfig:
        return SampleConfig.default()
    @property
    def reference_conf(self) -> SampleConfig:
        return SampleConfig.from_config(SampleConfig.reference_config(), use_reference_conf=False)
    
    def test_default_conf(self):
        conf = self.application_conf
        self.assertSampleConfigEqual(conf, self.expected_application_conf)
        self.assertNotEqual(conf, self.expected_reference_conf)
    
    def test_reference_conf(self):
        conf = self.reference_conf
        self.assertSampleConfigEqual(conf, self.expected_reference_conf)
        self.assertNotEqual(conf, self.expected_application_conf)
    
    def test_replace_should_keep_hidden_fields(self):
        params = \
        [
            ('Loaded config', self.application_conf),
            ('Instantiated config', self.expected_application_conf),
        ]
        
        for conf_name, conf in params:
            with self.subTest(conf_name=conf_name):
                replaced_conf = conf.replace(i1 = -40)
                
                # ToDo:
                #   It is known limitation that hidden fields are kept as-is
                self.assertIs(replaced_conf._config, conf._config)
                if (conf._config is None):
                    self.assertIsNone(replaced_conf._config)
                else:
                    self.assertIsNotNone(replaced_conf._config)
                    self.assertDictEqual(replaced_conf._config, conf._config)



__all__ = \
[
    'MainConfigClassTestCase',
]


if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
