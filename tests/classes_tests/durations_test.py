from datetime import timedelta as TimeDelta
from unittest import TestCase, main

from dataclasses_config import *


@config_class
class NestedConfig(Config):
    dur3: Duration

@main_config(reference_config_name='dur.conf')
class DurationConfig(MainConfig):
    dur: Duration
    dur2: Duration
    nested: NestedConfig

class DurationsTestCase(TestCase):
    def assertDuration(self, duration: Duration, expected_total_seconds: float):
        with self.subTest(duration=duration):
            self.assertIs(type(duration), Duration)
            self.assertIsInstance(duration, TimeDelta)
            self.assertEqual(duration.total_seconds(), expected_total_seconds)
    
    def test_parse_time(self):
        params = \
        [
            (TimeDelta(seconds=15), 15.0),
            (Duration(hours=6), 21600.0),
            (TimeDelta(hours=6), 21600.0),
            (1000,                1000.0),
            
            ('100ms',         0.1),
            ('100 millis',    0.1),
            ('1 millisecond', 0.001),
            ('2:53',        173.0),
            ('2:53:58',   10438.0),
            
            ('4 hours 51 minutes 13 milliseconds', 17460.013),
            ('4 days 5 hours', 363600.0),
            ('31 weeks',     18748800.0),
        ]
        for i, (arg, expected) in enumerate(params, start=1):
            with self.subTest(f"Test #{i}", arg=arg):
                self.assertDuration(Duration(arg), expected)
    
    def test_timedelta_keyword_constructor(self):
        params = \
        [
            (dict(minutes=2, seconds=53), 173.0),
            (dict(hours=2, minutes=53, seconds=58), 10438.0),
            (dict(hours=4, minutes=51, milliseconds=13), 17460.013),
            (dict(days=4, hours=5), 363600.0),
            (dict(weeks=31), 18748800.0),
        ]
        for i, (arg, expected) in enumerate(params, start=1):
            with self.subTest(f"Test #{i}", **arg):
                self.assertDuration(Duration(**arg), expected)
    
    def test_timedelta_deserialization(self):
        conf = DurationConfig.default()
        self.assertDuration(conf.dur, 5.0)
        self.assertDuration(conf.dur2, 0.015)
        self.assertDuration(conf.nested.dur3, 21600.0)


__all__ = \
[
    'DurationsTestCase',
]

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
