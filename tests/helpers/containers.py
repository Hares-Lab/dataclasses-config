from abc import abstractmethod, ABCMeta
from dataclasses import dataclass
from typing import *

from dataclasses_config import config_class, Config

T = TypeVar('T')
# region Simple containers
class TSimpleContainer(Config, Generic[T]):
    x: T
    default: T
    def __init_subclass__(cls, *, default: T = None, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.default = default

@config_class
class SimpleIntContainer(TSimpleContainer, default=0):
    x: int
@config_class
class SimpleStrContainer(TSimpleContainer):
    x: str
@config_class
class SimpleFloatContainer(TSimpleContainer, default=0.0):
    x: float
@config_class
class SimpleBoolContainer(TSimpleContainer, default=False):
    x: bool
# endregion
# region Multi-containers
class TContainerMeta(Generic[T], ABCMeta):
    @abstractmethod
    def _get_zeroes(cls) -> 'TMultiContainer[T]':
        raise NotImplementedError
    
    @property
    def zeroes(cls) -> 'TMultiContainer[T]':
        return cls._get_zeroes()

@dataclass
class TMultiContainer(Config, Generic[T], metaclass=TContainerMeta[T]):
    as_int: T
    as_float: T
    as_bool: T

@config_class
class IntMultiContainer(TMultiContainer[int]):
    as_int: int
    as_float: int
    as_bool: int
    
    @classmethod
    def _get_zeroes(cls) -> 'IntMultiContainer':
        return IntMultiContainer(0, 0, 0)

@config_class
class StringMultiContainer(TMultiContainer[str]):
    as_int: str
    as_float: str
    as_bool: str
    
    @classmethod
    def _get_zeroes(cls) -> 'StringMultiContainer':
        return StringMultiContainer('0', '0.0', 'false')

@config_class
class FloatMultiContainer(TMultiContainer[float]):
    as_int: float
    as_float: float
    as_bool: float
    
    @classmethod
    def _get_zeroes(cls) -> 'FloatMultiContainer':
        return FloatMultiContainer(0.0, 0.0, 0.0)

@config_class
class BoolMultiContainer(TMultiContainer[bool]):
    as_int: bool
    as_float: bool
    as_bool: bool
    
    @classmethod
    def _get_zeroes(cls) -> 'BoolMultiContainer':
        return BoolMultiContainer(False, False, False)
# endregion


__all__ = \
[
    'TContainerMeta',
    'TMultiContainer',
    'TSimpleContainer',

    'BoolMultiContainer',
    'FloatMultiContainer',
    'IntMultiContainer',
    'StringMultiContainer',
    
    'SimpleBoolContainer',
    'SimpleFloatContainer',
    'SimpleIntContainer',
    'SimpleStrContainer',
]
