from dataclasses import field
from enum import Enum, auto
from typing import *

from dataclasses_json import LetterCase
from dataclasses_json.cfg import config
from functional import AnyVal

from dataclasses_config import Config, config_class, Duration


@config_class
class Point(Config):
    x: float
    y: float

@config_class
class Circle(Config):
    center: Point
    radius: float

@config_class
class OptionalPoint(Point):
    x: int = 0
    y: int = 0
    
    def __eq__(self, other):
        if (isinstance(other, Point)):
            return (self.x, self.y) == (other.x, other.y)
        else:
            return super().__eq__(other)

@config_class
class Vector(Config):
    end: Point
    start: OptionalPoint

@config_class
class Line(Config):
    a: Optional[Point] = None
    b: Point = field(default_factory=lambda: Point(1, 1))

class Gender(Enum):
    Male = auto()
    Female = auto()

@config_class
class Person(Config):
    name: str
    age: int
    sex: Gender
    
    is_employed: bool = False
    job_title: Optional[str] = None
    works_for: Optional[Duration] = None
    salary: Optional[int] = None

class AnimalType(AnyVal[str]): pass

@config_class
class Animal(Config):
    # noinspection PyTypeChecker
    dataclass_json_config = config(letter_case=LetterCase.KEBAB)['dataclasses_json']
    
    tp: AnimalType = field(metadata=config(field_name='type'))
    name: str
    max_speed: Optional[float] = None
    hunts_for: List[AnimalType] = field(default_factory=list)


__all__ = \
[
    'Animal',
    'AnimalType',
    'Circle',
    'Gender',
    'Line',
    'OptionalPoint',
    'Person',
    'Point',
    'Vector',
]
