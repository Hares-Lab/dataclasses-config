import importlib.util
import os
from abc import ABC
from dataclasses import fields, Field
from typing import *
from unittest import TestCase

from functional import Option, Some

from dataclasses_config.config import HIDDEN_FIELD

C = TypeVar('C')
T = TypeVar('T')
class ConfigAssertionsMixin(TestCase, ABC):
    _old_wd: Option[str] = Option.empty
    
    @classmethod
    def setUpClass(cls):
        super(ConfigAssertionsMixin, cls).setUpClass()
        
        cls._old_wd = Some(os.curdir)
        os.chdir(os.path.dirname(os.path.abspath(importlib.util.find_spec(cls.__module__).origin)))
    
    @classmethod
    def tearDownClass(cls):
        super(ConfigAssertionsMixin, cls).tearDownClass()
        
        cls._old_wd.foreach(os.chdir)
        cls._old_wd = Option.empty
    
    def assertConfigEqual(self, actual: C, expected: C, *, cfg_cls: Type[C] = None):
        if (cfg_cls is None):
            cfg_cls = type(expected)
        
        for f in fields(cfg_cls): # type: Field
            if (f.init and not f.metadata.get(HIDDEN_FIELD, False)):
                asserter: Callable[[T, T, str], None]
                if (isinstance(f.type, List)):
                    asserter = self.assertListEqual
                elif (isinstance(f.type, Dict)):
                    asserter = self.assertDictEqual
                else:
                    asserter = self.assertEqual

                v_actual = getattr(actual, f.name)
                v_expected = getattr(expected, f.name)
                asserter(v_actual, v_expected, f"Wrong value for field {f.name!r}: Expected {v_expected!r}, Actual: {v_actual!r}")
        
        self.assertEqual(actual, expected)


__all__ = \
[
    'ConfigAssertionsMixin',
]
