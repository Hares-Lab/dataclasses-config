import sys
from typing import *
from unittest import TestCase, main, skipIf

from pyhocon import ConfigTree
from pyhocon.config_parser import ConfigFactory

from dataclasses_config.backports import cached_property
from dataclasses_config.errors import ConfigWrongPropertyTypeError
from tests.helpers.config_assertions import ConfigAssertionsMixin

if (sys.version_info >= (3, 7)):
    from .helpers.containers import *
else:
    TMultiContainer = None
    TSimpleContainer = None

# language=HOCON
CFG = \
"""
zeroes:
{
    as_int: 0
    as_float: 0.0
    as_bool: false
}

double_quoted_zeroes:
{
    as_int: "0"
    as_float: "0.0"
    as_bool: "false"
}

single_quoted_zeroes:
{
    as_int: '0'
    as_float: '0.0'
    as_bool: 'false'
}
"""

T = TypeVar('T')
@skipIf(sys.version_info < (3, 7), reason=f"ConfigFieldDataTypeTestCase is not supported on Python 3.6")
class ConfigFieldDataTypeTestCase(ConfigAssertionsMixin, TestCase):
    
    @cached_property
    def cfg(self) -> ConfigTree:
        return ConfigFactory.parse_string(CFG)
    
    # region Checkers
    def check_cls_ok_multi(self, cls: Type[TMultiContainer], *cases: str):
        for source in cases:
            with self.subTest(source=source):
                conf = cls.from_config(self.cfg.get_config(source))
                self.assertConfigEqual(conf, cls.zeroes)
    
    def check_cls_ok_simple(self, cls: Type[TSimpleContainer], *cases: str, only: Iterable):
        for source in cases:
            with self.subTest(source=source):
                for key, data in self.cfg.get_config(source).items():
                    if (any(x == data and type(x) == type(data) for x in only)):
                        with self.subTest(key=key, data=data, tp=type(data).__name__):
                            cfg = ConfigTree([ ('x', data) ])
                            conf = cls.from_config(cfg)
                            # noinspection PyArgumentList
                            self.assertConfigEqual(conf, cls(cls.default))
    
    def check_cls_not_ok(self, cls: Type[TSimpleContainer], *cases: str, ignore: Iterable = None):
        if (ignore is None): ignore = [ ]
        tp = cls.__annotations__['x']
        for source in cases:
            with self.subTest(source=source):
                for key, data in self.cfg.get_config(source).items():
                    with self.subTest(key=key, data=data, tp=type(data).__name__):
                        if (any(x == data and type(x) == type(data) for x in ignore)):
                            self.skipTest(f"Case {data!r} is valid")
                        
                        cfg = ConfigTree([ ('x', data) ])
                        with self.assertRaises(ConfigWrongPropertyTypeError) as cm:
                            cls.from_config(cfg)
                        
                        self.assertEqual(cm.exception.cls, cls)
                        self.assertEqual(cm.exception.key, 'x')
                        self.assertEqual(cm.exception.expected_type, tp)
                        self.assertEqual(cm.exception.actual_value, data)
    # endregion
    # region Positive tests
    def test_int_ok(self):
        self.check_cls_ok_multi(IntMultiContainer, 'zeroes')
        self.check_cls_ok_simple(SimpleIntContainer, 'double_quoted_zeroes', only=[ '0' ])
    def test_float_ok(self):
        self.check_cls_ok_multi(FloatMultiContainer, 'zeroes')
    def test_bool_ok(self):
        self.check_cls_ok_simple(SimpleBoolContainer, 'zeroes', 'double_quoted_zeroes', only=[ False, 'false' ])
    def test_str_ok(self):
        self.check_cls_ok_multi(StringMultiContainer, 'zeroes', 'double_quoted_zeroes')
    # endregion
    # region Negative tests
    def test_int_not_ok(self):
        self.check_cls_not_ok(SimpleIntContainer, 'double_quoted_zeroes', 'single_quoted_zeroes', ignore= [ '0' ])
    def test_float_not_ok(self):
        self.check_cls_not_ok(SimpleFloatContainer, 'double_quoted_zeroes', 'single_quoted_zeroes', ignore= [ '0', '0.0' ])
    def test_bool_not_ok(self):
        self.check_cls_not_ok(SimpleBoolContainer, 'zeroes', 'double_quoted_zeroes', 'single_quoted_zeroes', ignore= [ False, 'false' ])
    # endregion


__all__ = \
[
    'ConfigFieldDataTypeTestCase',
]

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
