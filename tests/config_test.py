import sys
from datetime import timedelta
from typing import *
from unittest import TestCase, main, skipIf

from pyhocon import ConfigTree, ConfigParser

from dataclasses_config import Config
from tests.helpers.classes import *
from tests.helpers.config_assertions import ConfigAssertionsMixin

# language=HOCON
PERSONS = \
"""
alice:
{
    name: "Alice"
    age: 25
    sex: Female
}

bob:
{
    name: "Bob"
    sex: Male
    age: 42
    is_employed: true
    job_title: "Sr. Memes Eng."
    salary: 10000
    works_for: 4 years
}
"""

# language=HOCON
ANIMALS = \
"""
lion:
{
    name: Lion
    type: cat
    max-speed: 80.5 # km/h
}

panther:
{
    name: Black Panther
    type: cat
}

wolf:
{
    name: Mr.Wolf
    type: dog
}
"""

C = TypeVar('C', bound=Config)

class ConfigClassTestCase(ConfigAssertionsMixin, TestCase):
    # region Persons
    @property
    def persons(self) -> ConfigTree:
        return ConfigParser.parse(PERSONS)
    
    @property
    def Alice(self) -> Person:
        return Person \
        (
            name = "Alice",
            age = 25,
            sex = Gender.Female,
            is_employed = False,
            job_title = None,
            works_for = None,
            salary = None,
        )
    
    @property
    def Bob(self) -> Person:
        return Person \
        (
            name = "Bob",
            age = 42,
            sex = Gender.Male,
            is_employed = True,
            job_title = "Sr. Memes Eng.",
            # This may fail on years 2395-2400
            works_for = timedelta(days=365*4 + 1),
            salary = 10000,
        )
    # endregion
    # region Animals
    @property
    def animals(self) -> ConfigTree:
        return ConfigParser.parse(ANIMALS)
    
    @property
    def Lion(self) -> Animal:
        return Animal \
        (
            tp = AnimalType('cat'),
            name = "Lion",
            max_speed = 80.5,
        )
    @property
    def Panther(self) -> Animal:
        return Animal \
        (
            tp = AnimalType('cat'),
            name = "Black Panther",
            max_speed = None,
        )
    @property
    def Wolf(self) -> Animal:
        return Animal \
        (
            tp = AnimalType('dog'),
            name = "Mr.Wolf",
            max_speed = None,
        )
    # endregion
    
    def check_configs_many(self, configs: ConfigTree, tp: Type[Config], options: Iterable[Tuple[str, Config]]):
        for name, expected in options:
            with self.subTest(name=name):
                self.assertConfigEqual(tp.from_config(configs.get_config(name)), expected)
    
    def test_config_with_defaults(self):
        options = \
        [
            ('alice', self.Alice),
        ]
        
        self.check_configs_many(self.persons, Person, options)
    
    def test_config_without_defaults(self):
        options = \
        [
            ('bob', self.Bob),
        ]
        
        self.check_configs_many(self.persons, Person, options)
    
    @skipIf(sys.version_info < (3, 7), reason="functional.AnyVal integration with dataclasses_json is not supported for Python 3.6")
    def test_dcj_field_name(self):
        options = \
        [
            ('lion', self.Lion),
            ('panther', self.Panther),
            ('wolf', self.Wolf),
        ]
        
        self.check_configs_many(self.animals, Animal, options)
    
    def test_complex_defaults(self):
        data: List[Tuple[Type[C], Callable[[], ConfigTree], Callable[[], C]]] = \
        [
            # (
            #     OptionalPoint,
            #     lambda: ConfigTree(),
            #     lambda: Point(0, 0),
            # ),
            (
                Vector,
                lambda: ConfigTree(end=ConfigTree(x=3, y=4)),
                lambda: Vector(Point(3, 4), Point(0, 0)),
            ),
            # (
            #     Vector,
            #     lambda: ConfigTree(end=ConfigTree(x=5, y=6), start=ConfigTree()),
            #     lambda: Vector(Point(5, 6), Point(0, 0)),
            # ),
            # (
            #     Line,
            #     lambda: ConfigTree(),
            #     lambda: Line(None, Point(1, 1)),
            # ),
            # (
            #     Line,
            #     lambda: ConfigTree(a=ConfigTree(x=7, y=8)),
            #     lambda: Line(Point(7, 8), Point(1, 1)),
            # ),
        ]
        
        for cls, src_gen, expected_gen in data:
            with self.subTest(cls=cls.__name__):
                src = src_gen()
                expected = expected_gen()
                
                actual = cls.from_config(src)
                self.assertEqual(actual, expected)


__all__ = \
[
    'ConfigClassTestCase',
]


if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
